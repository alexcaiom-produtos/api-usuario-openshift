package br.com.massuda.alexander.usuario.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.massuda.alexander.spring.framework.infra.controller.Controlador;
import br.com.massuda.alexander.usuario.orm.bo.BOUsuario;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.Usuario;

@CrossOrigin
@Controller
@RequestMapping("/usuario")
public class UsuarioController extends Controlador<Usuario> {
	
	@Autowired
	private BOUsuario bo;

	@PostMapping
	public void incluir(Usuario usuario) {
		/*if (existe(usuario.getFotos()) && !usuario.getFotos().isEmpty()) {
			try {
				String raiz = System.getProperty("catalina.home");
				File pastaArquivosTemporarios = new File(raiz + "/temp/");
				if (!pastaArquivosTemporarios.exists()) {
					pastaArquivosTemporarios.mkdirs();
				}
				try{
					String storage = "/storage/";
					File caminhoArquivo = new File(raiz+storage);
					if (!caminhoArquivo.exists()) {
						caminhoArquivo.mkdirs();
					}
					caminhoArquivo = caminhoArquivo + "/";

					while (i.hasNext()) {
						FileItem item = i.next();
						if (!item.isFormField()) {
							String nomeCampo = item.getFieldName();
							String nomeArquivo = item.getName();
							String tipoConteudo = item.getContentType();
							boolean estaEmMemoria = item.isInMemory();
							long tamanhoEmBytes = item.getSize();

							if (nomeArquivo.lastIndexOf("/") >= 0) {
								arquivo = new File(caminhoArquivo + nomeArquivo.substring(nomeArquivo.lastIndexOf("\\")));
							} else {
								arquivo = new File(caminhoArquivo + nomeArquivo.substring(nomeArquivo.lastIndexOf("\\")+1));
							}

							item.write(arquivo);
							Foto foto = new Foto();
							foto.setCaminho(arquivo.getAbsolutePath());
							foto.setData(GregorianCalendar.getInstance());
							foto.setDescricao(arquivo.getName());
							fotos.add(foto);
						} else {
						}
					}
				} catch (Exception e) {
					getWriter(null, resposta).println("Erro: "+e.getMessage());
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}*/
		
		bo.incluir(usuario);
	}
	
	@ResponseBody
	@GetMapping("/id/{id}")
	public Usuario pesquisar (@PathVariable Long id) {
		return bo.pesquisar(id);
	}

	@ResponseBody
	@GetMapping("/nome/{nome}")
	public List<Usuario> pesquisarPorNome (@PathVariable String nome) {
		return bo.pesquisarPorNome(nome);
	}

	@ResponseBody
	@GetMapping("/login/{login}")
	public Usuario pesquisar (@PathVariable String login) {
		return bo.pesquisarPorLogin(login);
	}
	
	@ResponseBody
	@GetMapping
	public List<Usuario> listar () {
		return bo.listarUsuarios();
	}

}
