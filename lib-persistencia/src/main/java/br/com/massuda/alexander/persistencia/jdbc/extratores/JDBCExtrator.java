/**
 * 
 */
package br.com.massuda.alexander.persistencia.jdbc.extratores;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import br.com.waiso.framework.abstratas.Classe;

/**
 * @author Alex
 *
 */
public abstract class JDBCExtrator extends Classe {
	
	abstract void extrair(PreparedStatement comandoPreparado, Class tipo, Object valor, int indice) throws SQLException;

}
