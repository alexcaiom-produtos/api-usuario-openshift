/**
 * 
 */
package br.com.massuda.alexander.persistencia.jdbc.extratores;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @author Alex
 *
 */
public class ExtratorTipoEnum extends JDBCExtrator  {

	@Override
	void extrair(PreparedStatement comandoPreparado, Class tipo, Object valor, int indice) throws SQLException {
		comandoPreparado.setString(indice, 		(existe(valor) ? ((Enum)valor).name() : null));		
	}

}
