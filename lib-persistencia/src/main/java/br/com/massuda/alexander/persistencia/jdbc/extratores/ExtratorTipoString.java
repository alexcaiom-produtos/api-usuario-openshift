/**
 * 
 */
package br.com.massuda.alexander.persistencia.jdbc.extratores;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @author Alex
 *
 */
public class ExtratorTipoString extends JDBCExtrator {
	

	public void extrair(PreparedStatement comandoPreparado, Class tipo, Object valor, int indice) throws SQLException {
		comandoPreparado.setString(indice, 		(String) valor);
	}

}
