/**
 * 
 */
package br.com.massuda.alexander.persistencia.jdbc.extratores;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

import br.com.waiso.framework.abstratas.Classe;

/**
 * @author Alex
 *
 */
public class ExtratorDeTipos extends Classe {

	private List<JDBCExtrator> extratores;
	
	public ExtratorDeTipos() {
		extratores = new ArrayList<>();
		extratores.add(new ExtratorTipoBoolean());
		extratores.add(new ExtratorTipoData());
		extratores.add(new ExtratorTipoDouble());
		extratores.add(new ExtratorTipoEnum());
		extratores.add(new ExtratorTipoFloat());
		extratores.add(new ExtratorTipoInteiro());
		extratores.add(new ExtratorTipoLong());
		extratores.add(new ExtratorTipoString());
	}

	public void setParametroDeComandoPreparadoPorTipo(PreparedStatement comandoPreparado, Class tipo, Object valor, int indice) throws SQLException {
		JDBCExtrator extrator = JDBCTipos.selecionar(extratores, tipo);
		extrator.extrair(comandoPreparado, tipo, valor, indice);
	}
	
}
