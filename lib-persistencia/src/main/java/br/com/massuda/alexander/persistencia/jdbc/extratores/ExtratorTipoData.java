/**
 * 
 */
package br.com.massuda.alexander.persistencia.jdbc.extratores;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;

/**
 * @author Alex
 *
 */
public class ExtratorTipoData extends JDBCExtrator {
	

	public void extrair(PreparedStatement comandoPreparado, Class tipo, Object valor, int indice) throws SQLException {
		java.sql.Date dataBD = null;
		if (tipo == Calendar.class) {
			Calendar data = (Calendar)valor;
			dataBD = getDataBD(data);
		} else if (tipo == Date.class) {
			Date data = (Date)valor;
			dataBD = getDataBD(data); 
		} else if (tipo == LocalDate.class) {
			LocalDate data = (LocalDate) valor;
			dataBD = getDataBD(data);
		} else if (tipo == LocalDateTime.class) {
			LocalDateTime hora = (LocalDateTime) valor;
			try {
				dataBD = getDataBD(hora);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		comandoPreparado.setDate(indice, dataBD);
	}
	
	private java.sql.Date getDataBD (Calendar data){
		return new java.sql.Date(data.getTimeInMillis());
	}
	
	private java.sql.Date getDataBD (Date data){
		return new java.sql.Date(data.getTime());
	}
	
	private java.sql.Date getDataBD (LocalDate data){
		return new java.sql.Date(data.toEpochDay());
	}
	
	private java.sql.Date getDataBD (LocalDateTime hora) throws ParseException{
		try {
			if (null != hora) {
				Date dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(hora.toString().replaceAll("T", " "));
				return new java.sql.Date(dt.getTime());
			}
			return null;
		} catch (ParseException e) {
			throw e;
		}
	}

}
