package br.com.massuda.alexander.persistencia.jdbc;

import java.lang.reflect.Field;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

import br.com.massuda.alexander.persistencia.interfaces.IDAO;
import br.com.massuda.alexander.persistencia.jdbc.excecoes.RegistroJaExistenteException;
import br.com.massuda.alexander.persistencia.jdbc.model.EntidadeModelo;
import br.com.waiso.framework.abstratas.Classe;
import br.com.waiso.framework.exceptions.ErroUsuario;

public abstract class DAOGenericoJDBCImpl<T> extends DAOGenericoJDBCImplUtils<T> implements IDAO<T> {
	
	protected Class<T> entidade;
	
	public DAOGenericoJDBCImpl(Class<T> entidade) {
		this.entidade = entidade;
		this.possuiHeranca = verificaSePossuiHeranca(entidade);
	}

	public T incluir (T o) throws ErroUsuario {
		tipoCRUD = OperacaoCRUD.INSERCAO;
		String sql = GeradorSQLBean.getInstancia(o.getClass()).getComandoInsercao(o);
		log(sql);
		try {
			comandoPreparado = novoComandoPreparado(sql, RETORNAR_ID_GERADO);
			
			preencherParametrosDeComandoDeInsercao(comandoPreparado, o);

			ResultSet retornoChavesGeradas = this.executarOperacaoERetornarChavesGeradas(comandoPreparado);
			if (temAtributoIdEditavel(o)) {
				if (retornoChavesGeradas.next()) {
					Long chaveGerada = retornoChavesGeradas.getLong(1);
					setarId(o, chaveGerada, null);
				}
			}
			retornoChavesGeradas.close();
		} catch (MySQLIntegrityConstraintViolationException e) {
			throw new RegistroJaExistenteException(e);
		} catch (SQLException e) {
			e.printStackTrace();
		}  finally {
			try {
				fechar();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return o;
	}
	
	public ResultSet executarOperacaoERetornarChavesGeradas(PreparedStatement comandoPreparado) throws SQLException {
		if (null != comandoPreparado) {
			try {
				comandoPreparado.execute();
				return comandoPreparado.getGeneratedKeys();
			} catch (Exception e) {
				if (e.getMessage().contains("Table") && e.getMessage().contains("doesn't exist")) {
					criarTabelaEDependencias();
					comandoPreparado.execute();
					return comandoPreparado.getGeneratedKeys();
				}
			}
		}
		return null;
	}

	/**
	 */
	private void criarTabelaEDependencias() {
		criarDependencias(entidade);
		
		String sqlCriacao = GeradorSQLBean.getInstancia(entidade).getComandoTabelaCriacao();
		System.out.println("SQL de Criacao de "+entidade.getSimpleName()+": "+sqlCriacao);
		try {
			getConexao().prepareStatement(sqlCriacao).execute();
		} catch (SQLException e2) {
			e2.printStackTrace();
		}
	}

	private void criarDependencias(Class<T> entidade) {
		List<Field> camposFK = GeradorSQLBean.getInstancia(entidade).getCamposFK();
		
		
		for (Field campo : camposFK) {
			if (!existeTabela(campo) && GeradorSQLBean.getInstancia(campo.getType()).deveCriarTabela()) {
				String sqlCriacao = GeradorSQLBean.getInstancia(campo.getType()).getComandoTabelaCriacao();
				try {
					getConexao().prepareStatement(sqlCriacao).execute();
				} catch (SQLException e2) {
					e2.printStackTrace();
				}
			}
		}
		
	}

	private boolean existeTabela(Field campo) {
		String sqlSelect = GeradorSQLBean.getInstancia(campo.getType()).getComandoSelecao();
		try {
			getConexao().createStatement().executeQuery(sqlSelect);
			return true;
		}catch (Exception e) {
			return false;
		}
	}

	public void editar (T o) {

		String sql = GeradorSQLBean.getInstancia(entidade).getComandoAtualizacao(o);
		log(sql);
		try {
			comandoPreparado =  novoComandoPreparado(sql);
			preencherEdicoes(comandoPreparado, o);
			
			preencherFiltros(comandoPreparado, o, null);
			executarOperacaoParametrizada(comandoPreparado);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				fechar();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void excluir(T o) throws ErroUsuario {
		if (Classe.naoExiste(o)) {
			throw new ErroUsuario("Objeto nao informado");
		}
		String sql = GeradorSQLBean.getInstancia(entidade).getComandoExclusao();
		sql += "\n where id = ?";
		log(sql);
		try {
			comandoPreparado = novoComandoPreparado(sql);
			preencherExclusao(comandoPreparado, o);
			executarOperacaoParametrizada(comandoPreparado);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				fechar();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void excluir(long id) throws ErroUsuario {
		String sql = GeradorSQLBean.getInstancia(entidade).getComandoExclusao();
		sql += "\n where id = ?";
		log(sql);
		try {
			comandoPreparado = novoComandoPreparado(sql);
			comandoPreparado.setLong(1, id);
			executarOperacaoParametrizada(comandoPreparado);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				fechar();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void excluir(EntidadeModelo objeto) throws ErroUsuario {
		verificaSeOModeloEstaPreenchido(objeto);
		
		excluir(objeto.getId());
	}
	
	


}
