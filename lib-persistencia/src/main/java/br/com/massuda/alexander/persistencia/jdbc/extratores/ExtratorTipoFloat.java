/**
 * 
 */
package br.com.massuda.alexander.persistencia.jdbc.extratores;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @author Alex
 *
 */
public class ExtratorTipoFloat extends JDBCExtrator {
	

	public void extrair(PreparedStatement comandoPreparado, Class tipo, Object valor, int indice) throws SQLException {
		comandoPreparado.setFloat(indice, 		(existe(valor) ? (Float) valor : null));
	}

}
