/**
 * 
 */
package br.com.massuda.alexander.persistencia.jdbc.extratores;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author Alex
 *
 */
public enum JDBCTipos {
	
	INTEIRO(ExtratorTipoInteiro.class, 	Integer.class, int.class),
	Long (	ExtratorTipoLong.class, 	Long.class, long.class),
	Double (ExtratorTipoDouble.class, 	Double.class, double.class),
	Float (	ExtratorTipoFloat.class, 	Integer.class, float.class),
	Boolean(ExtratorTipoBoolean.class, 	Integer.class, boolean.class),
	String (ExtratorTipoString.class, 	String.class),
	Enum (	ExtratorTipoEnum.class, 	(Class[])null),
	Data (	ExtratorTipoData.class, 	Calendar.class, Date.class, LocalDate.class, LocalDateTime.class);
	
	private Class extrator;
	private Class[] classes;

	private JDBCTipos(Class<? extends JDBCExtrator> extrator,  Class... classes) {
		this.extrator = extrator;
		this.classes = classes;
	}

	public Class getExtrator() {
		return extrator;
	}

	public void setExtrator(Class<? extends JDBCExtrator> extrator) {
		this.extrator = extrator;
	}

	public Class[] getClasses() {
		return classes;
	}

	public void setClasses(Class[] classes) {
		this.classes = classes;
	}
	
	public static Class get(Class tipo) {
		for (JDBCTipos extrator : values()) {
			for (Class tipoDisponivel : extrator.getClasses()) {
				if (tipoDisponivel.equals(tipo)) {
					return extrator.getExtrator();
				}
			}
		}
		throw new IllegalArgumentException("Tipo invalido");
	}
	
	public static JDBCExtrator selecionar(List<JDBCExtrator> extratores, Class tipo) {
		Class selecionado = get(tipo);
		for (JDBCExtrator extrator : extratores) {
 			if (selecionado.equals(extrator)) {
				return extrator;
			}
		}
		throw new IllegalArgumentException("Tipo invalido");
	}

}
