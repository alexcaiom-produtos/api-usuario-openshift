package br.com.massuda.alexander.usuario.orm.modelo;

public enum StatusDeUsuario {

	OK,
	BLOQUEADO,
	BLOQUEADO_POR_SENHA_INVALIDA;
	
}
