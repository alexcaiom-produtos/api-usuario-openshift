package br.com.massuda.alexander.usuario.orm.modelo;

public enum TipoPessoa {

	PESSOA_FISICA,
	PESSOA_JURIDICA
	
}
