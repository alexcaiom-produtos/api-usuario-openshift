package br.com.massuda.alexander.usuario.orm.modelo;

public enum NivelHierarquico {

	ADMINISTRADOR,
	COORDENADOR,
	DIRETOR,
	GERENTE,
	OPERADOR,
	PRESIDENTE,
	SUPERVISOR,
	USUARIO,
	VICE_PRESIDENTE;
	
}
