package br.com.massuda.alexander.usuario.orm.modelo;

public enum Operadora {

	CLARO,
	NEXTEL,
	OI,
	TIM,
	VIVO
	
}
