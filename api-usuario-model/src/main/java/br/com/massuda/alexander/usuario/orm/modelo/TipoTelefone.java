package br.com.massuda.alexander.usuario.orm.modelo;

public enum TipoTelefone {

	RESIDENCIAL,
	COMERCIAL,
	CELULAR
	
}
