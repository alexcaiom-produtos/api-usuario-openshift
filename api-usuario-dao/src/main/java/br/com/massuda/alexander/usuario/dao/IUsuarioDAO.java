package br.com.massuda.alexander.usuario.dao;

import br.com.massuda.alexander.persistencia.interfaces.IDAO;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.Usuario;

public interface IUsuarioDAO extends IDAO<Usuario> {

}
