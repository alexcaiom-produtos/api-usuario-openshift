package br.com.massuda.alexander.usuario.dao.finder;

import br.com.massuda.alexander.persistencia.interfaces.IFinder;
import br.com.massuda.alexander.usuario.orm.modelo.sistema.Sistema;

public interface IFinderSistema extends IFinder<Long, Sistema> {
	
}
