package br.com.massuda.alexander.usuario.dao.finder.usuario;

import br.com.massuda.alexander.persistencia.interfaces.IFinder;
import br.com.massuda.alexander.usuario.orm.modelo.Endereco;

public interface IFinderEndereco extends IFinder<Long, Endereco> {
	
	
}
