package br.com.massuda.alexander.usuario.dao.finder.usuario.profissional;

import br.com.massuda.alexander.persistencia.interfaces.IFinder;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.profissional.ExperienciaProfissional;

public interface IFinderExperienciaProfissional extends IFinder<Long, ExperienciaProfissional> {
	
}
