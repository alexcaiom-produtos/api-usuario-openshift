package br.com.massuda.alexander.usuario.dao;

import br.com.massuda.alexander.persistencia.interfaces.IDAO;
import br.com.massuda.alexander.usuario.orm.modelo.Permissao;

public interface IPermissaoDAO extends IDAO<Permissao> {

}
