package br.com.massuda.alexander.usuario.dao.finder.impl;

import br.com.massuda.alexander.persistencia.jdbc.finder.FinderJDBC;
import br.com.massuda.alexander.usuario.dao.impl.DAO;

public abstract class Finder<T> extends FinderJDBC<T> {

	public Finder(Class<T> entidade) {
		super(entidade);
		DAO.configurarBancoDeDadosDadosAcesso();
	}

}
