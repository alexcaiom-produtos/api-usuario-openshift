package br.com.massuda.alexander.usuario.dao.finder;

import br.com.massuda.alexander.persistencia.interfaces.IFinder;
import br.com.massuda.alexander.usuario.orm.modelo.Permissao;

public interface IFinderPermissao extends IFinder<Long, Permissao> {
	
	Permissao pesquisarPorUsuario (Long idUsuario);
	
}
