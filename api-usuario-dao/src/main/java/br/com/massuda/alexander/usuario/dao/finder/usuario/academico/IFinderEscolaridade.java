package br.com.massuda.alexander.usuario.dao.finder.usuario.academico;

import br.com.massuda.alexander.persistencia.interfaces.IFinder;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.academico.Escolaridade;

public interface IFinderEscolaridade extends IFinder<Long, Escolaridade> {
	
}
