package br.com.massuda.alexander.usuario.orm.bo;

import java.util.List;

import br.com.massuda.alexander.spring.framework.infra.excecoes.Erro;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.Usuario;

public interface BOUsuario extends IBO<Long, Usuario> {

	public List<Usuario> listarUsuarios() throws Erro;
	public Usuario incluir(Usuario u)  throws Erro ;
	public List<Usuario> pesquisarPorNome(String nome) throws Erro ;
	public Usuario pesquisarPorLogin(String nome);
	
}
