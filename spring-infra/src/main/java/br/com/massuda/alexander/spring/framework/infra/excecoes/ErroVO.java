package br.com.massuda.alexander.spring.framework.infra.excecoes;

import java.io.Serializable;

/**
 * @author Alex
 * Classe Mãe de Erros/Exceções
 */
public class ErroVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6764654263802020868L;
	public String erro;
	public String mensagem;
	
	public ErroVO() {}
	public ErroVO(Erro e){
		this.erro = e.getErro();
	}
	
	public void setErro(String erro) {
		this.erro = erro;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

}