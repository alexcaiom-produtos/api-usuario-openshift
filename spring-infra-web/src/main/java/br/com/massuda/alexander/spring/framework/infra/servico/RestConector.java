package br.com.massuda.alexander.spring.framework.infra.servico;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import javax.annotation.PostConstruct;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import br.com.massuda.alexander.spring.framework.infra.excecoes.Erro;

@Component
public class RestConector<T> extends Conector<T> {

	private Class<T> tipo;
	
//	@PostConstruct
//	public void init() {
//		ParameterizedType genericSuperclass = (ParameterizedType) this.getClass().getGenericSuperclass();
//		Type actualTypeArgument = genericSuperclass.getActualTypeArguments()[0];
//		 this.tipo = (Class<T>) actualTypeArgument.getClass(); //(Class<T>)((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0];
//	}
	
	/**
	 * Acesso via GET
	 * @param url
	 * @return
	 */
	public T getForEntity(String url) {
		ResponseEntity<T> resposta = restTemplate.getForEntity(url, tipo);
		return tratarResposta(resposta);
	}
	
	/**
	 * 
	 * @param url
	 * @param tipo
	 * @return
	 */
	public T getForEntity(String url, Class tipo) {
		ResponseEntity<T> resposta = restTemplate.getForEntity(url, tipo);
		return tratarResposta(resposta);
	}
	
	public T getForObject(String url) {
		 T list = (T) restTemplate.getForObject(url, tipo);
		return list;
	}
	
	public Object getForObject(String url, Class tipo) {
		Object obj = restTemplate.getForObject(url, tipo);
		return obj;
	}
	
	/**
	 * 
	 * @param url
	 * @param o
	 * @param tipo
	 * @return
	 */
	public T postForEntity(String url, T o, Class tipo) {
		ResponseEntity<T> resposta = restTemplate.postForEntity(url, o, tipo);
		return tratarResposta(resposta);
	}
	
	/**
	 * 
	 * @param url
	 * @param o
	 * @return
	 */
	public T postForEntity(String url, T o) {
		ResponseEntity<T> resposta = restTemplate.postForEntity(url, o, tipo);
		return tratarResposta(resposta);
	}

	private boolean sucesso(ResponseEntity<T> resposta) {
		return resposta.getStatusCode().is2xxSuccessful() ||
				resposta.getStatusCode().is1xxInformational();
	}
	
	private T tratarResposta(ResponseEntity<T> resposta) {
		if (!sucesso(resposta)) {
			tratarHttpStatusErro(resposta);
		}
		return resposta.getBody();
	}
	
	private void tratarHttpStatusErro(ResponseEntity<T> resposta) {
		Erro erro = (Erro) resposta.getBody();
		throw erro;
	}

	public Class<T> getTipo() {
		return tipo;
	}

	public void setTipo(Class<T> tipo) {
		this.tipo = tipo;
	}
	
	
}
