/**
 * 
 */
package br.com.massuda.alexander.spring.framework.infra.web.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import br.com.massuda.alexander.spring.framework.infra.servico.RestConector;

/**
 * @author Alex
 *
 */
@Configuration
public class Configuracao {

	@Bean
	public RestConector getRestConector() {
		return new RestConector();
	}
	
	@Bean
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}

}
